#include <windows.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include<math.h>
#include <stdlib.h>
#include<stdio.h>
#define PI acos(-1.0)

//static GLfloat spin = 0.0;
static GLfloat v_boat = 70.0,v_cloud_r=40.0,v_cloud_l=-40.0,v_fan=360,v_back_ghop=42,fish_y=-30,fish_x=40;
static GLfloat flag = 1.0;


void circle(GLdouble rad)
{
	GLint points = 50;
	GLdouble delTheta = (2.0 * PI) / (GLdouble)points;
	GLdouble theta = 0.0;

	glBegin(GL_POLYGON);
	{
		for( GLint i = 0; i <=50; i++, theta += delTheta )
		{
			glVertex2f(rad * cos(theta),rad * sin(theta));
		}
	}
	glEnd();
}

void half_circle(GLdouble rad)
{
	GLint points = 50;
	GLdouble delTheta = (2.0 * PI) / (GLdouble)points;
	GLdouble theta = 0.0;

	glBegin(GL_POLYGON);
	{
		for( GLint i = 0; i <=25; i++, theta += delTheta )
		{
			glVertex2f(rad * cos(theta),rad * sin(theta));
		}
	}
	glEnd();
}

void rect()
{

	glPushMatrix() ;
	glBegin(GL_POLYGON);

	  glColor3f(1,1,1);
      glVertex3f (0, 0, 0.0);
	  glColor3f(0.196078,0.6,0.8);
      glVertex3f (0, 2, 0.0);
	  glColor3f(0.196078,0.6,0.8);
	  glVertex3f (1.5, 2, 0.0);
	  glColor3f(0.196078,0.6,0.8);
	  glVertex3f (1.5, 0, 0.0);

	glEnd();
	glPopMatrix() ;

}

void rect_b()
{

	glPushMatrix() ;
	glBegin(GL_POLYGON);


      glVertex3f (0, 0, 0.0);
	  //glColor3f(0.196078,0.6,0.8);
      glVertex3f (0, 2, 0.0);
	  //glColor3f(0.196078,0.6,0.8);
	  glVertex3f (1.5, 2, 0.0);
	  //glColor3f(0.196078,0.6,0.8);
	  glVertex3f (1.5, 0, 0.0);

	glEnd();
	glPopMatrix() ;

}


void home()
{
	//chal
	glPushMatrix() ;
	glBegin(GL_POLYGON);

    glColor3f(0.556863, 0.557255, 0.257255);
	glVertex3f (0, 0, 0.0);
    glVertex3f (18, 14, 0.0);
    glVertex3f (36, 0, 0.0);

    glEnd();
	glPopMatrix();

	//ghor
	glPushMatrix() ;
	glBegin(GL_POLYGON);

    glColor3f(.73456, .3053 ,0.1098);
	glVertex3f(2, 0, 0.0);
    glVertex3f (2, -20, 0.0);
    glVertex3f (34, -20, 0.0);
    glVertex3f (34, 0, 0.0);

    glEnd();
	glPopMatrix();

	//siri
	glPushMatrix() ;
	glBegin(GL_POLYGON);

    glColor3f(1.0,0.25,0.0 );
	glVertex3f (0, -22, 0.0);
    glVertex3f (2, -20, 0.0);
    glVertex3f (34, -20, 0.0);
    glVertex3f (36, -22, 0.0);

    glEnd();
	glPopMatrix();

	//door
	glPushMatrix() ;
	glBegin(GL_POLYGON);

    glColor3f(0.42,0.26,0.15);
	glVertex3f (13, -20, 0.0);
    glVertex3f (13, -10, 0.0);
    glVertex3f (21, -10, 0.0);
    glVertex3f (21, -20, 0.0);

    glEnd();

    //stick
	glPushMatrix() ;
	glBegin(GL_POLYGON);

    glColor3f(0,0,0);
	glVertex3f (-8, 10, 0.0);
    glVertex3f (-11, 10, 0.0);
    glVertex3f (-11, -22, 0.0);
    glVertex3f (-8, -22, 0.0);

    glEnd();

	glPopMatrix();




}





void cloud()

{
	//left

	glPushMatrix();
	glTranslatef(4,5.5,0);
	circle(4);
	glPopMatrix();

	//right

	glPushMatrix();
	glTranslatef(-8,5.5,0);
	circle(3.5);
	glPopMatrix();

	//top left

	glPushMatrix();
	glTranslatef(-3.5,9,0);
	circle(3.5);
	glPopMatrix();

	//top right

	glPushMatrix();
	glTranslatef(1,9,0);
	circle(3);
	glPopMatrix();

	//middle

	glPushMatrix();

	//glColor3f (1, 1 ,1);
	glTranslatef(-1.5,5.5,0);
	circle(4);
	glPopMatrix();

}

void boat()
{
	//shawni
	glPushMatrix() ;
	glBegin(GL_POLYGON);

    glColor3f(0.556863, 0.137255, 0.137255);
	glVertex3f (0, 0, 0.0);
    glVertex3f (4, -6, 0.0);
    glVertex3f (32, -6, 0.0);
    glVertex3f (36, 0, 0.0);


    glBegin(GL_QUADS);
    glColor3f(0, 0, 1);
	glVertex3f (10, 0, 0.0);
    glVertex3f (20, 6, 0.0);
    glVertex3f (28, 6, 0.0);
    glVertex3f (33, 0, 0.0);

    glEnd();


	glPopMatrix();


}

void triangle(void)
{
	glColor3f (0.137255,0.556863,0.137255);
	glBegin(GL_POLYGON);

	  glVertex3f (0, 0, 0.0);
      glVertex3f (9, 13, 0.0);
	  glVertex3f (18, 0, 0.0);

	glEnd();
}
void jhop()
{
	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(50,10,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(35,10,0);
	cloud();
	glPopMatrix();


	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(28,16,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(20,10,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(28,13,0);
	glScalef(.1,.1,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(20,23,0);
	glScalef(.1,.1,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(38,16,0);
	glScalef(.1,.1,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.309804,0.184314,0.309804);
	glTranslatef(50,14,0);
	glScalef(.1,.1,0);
	cloud();
	glPopMatrix();

}

void tree()
{
	glPushMatrix();
	glColor3f (0.137255,0.556863,0.137255);
	glTranslatef(14,2,0);
    cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.137255,0.556863,0.137255);
	glTranslatef(15,7,0);
	cloud();
	glPopMatrix();

	glPushMatrix();
	glColor3f (0.137255,0.556863,0.137255);
	glTranslatef(12,9,0);
	cloud();
	glPopMatrix();

    glPushMatrix();
	glColor3f (0.137255,0.556863,0.137255);
	glTranslatef(17,9,0);
	cloud();
	glPopMatrix();


	//gora

	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0.36,0.25,0.20);
	glVertex3f (10, 5, 0.0);
    glVertex3f (10, -4, 0.0);
    glVertex3f (14, -4, 0.0);
    glVertex3f (14, 5, 0.0);

    glEnd();
	glPopMatrix();

}

void road()
{
	//1
	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0,0,.1);
	glVertex3f (0, 0, 0.0);
    glVertex3f (0, 3, 0.0);
    glVertex3f (25, 3, 0.0);
    glVertex3f (25, 0, 0.0);

    glEnd();
	glPopMatrix();

	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0,0,.5);
	glVertex3f (0, 3, 0.0);
    glVertex3f (0, 4.5, 0.0);
    glVertex3f (20, 4.5, 0.0);
    glVertex3f (25, 3, 0.0);

    glEnd();
	glPopMatrix();

	//2
	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0,0,.5);
	glVertex3f (25, 0, 0.0);
    glVertex3f (25, 3, 0.0);
    glVertex3f (45, 3, 0.0);
    glVertex3f (45, 0, 0.0);

    glEnd();
	glPopMatrix();

	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0,0,.5);
	glVertex3f (25, 3, 0.0);
    glVertex3f (20, 4.5, 0.0);
    glVertex3f (40, 4.5, 0.0);
    glVertex3f (45, 3, 0.0);

    glEnd();
	glPopMatrix();

	//3
	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0,0,.5);
	glVertex3f (45, 0, 0.0);
    glVertex3f (45, 3, 0.0);
    glVertex3f (65, 3, 0.0);
    glVertex3f (65, 0, 0.0);

    glEnd();
	glPopMatrix();

	glPushMatrix();
	glBegin(GL_POLYGON);

    glColor3f(0,0,.5);
	glVertex3f (45, 3, 0.0);
    glVertex3f (40, 4.5, 0.0);
    glVertex3f (60, 4.5, 0.0);
    glVertex3f (65, 3, 0.0);

    glEnd();
	glPopMatrix();




}

void ground()
{


	//gray
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();
	glTranslatef(5,-2,0);
	glBegin(GL_POLYGON);

	glVertex3f (-100, -5, 0.0);
    glVertex3f (-100,5, 0.0);
    glVertex3f (100,5, 0.0);
    glVertex3f (100,-5, 0.0);

    glEnd();
	glPopMatrix();



//green

	glColor3f(0.6,0.8,0.196078);
	glPushMatrix();
	glTranslatef(0,-1,0);
	glBegin(GL_POLYGON);

	glVertex3f (-100, -5, 0.0);
    glVertex3f (-100,20, 0.0);
    glVertex3f (100,20, 0.0);
    glVertex3f (100,-5, 0.0);

    glEnd();
	glPopMatrix();

//golden

    glColor3f(0.8,0.498039,0.196078);
	glPushMatrix();
	glTranslatef(-70,-42,0);
	glBegin(GL_POLYGON);

	glVertex3f (-10, 0, 0.0);
    glVertex3f (-10,10, 0.0);
    glVertex3f (600,10, 0.0);
    glVertex3f (600,0, 0.0);

    glEnd();
	glPopMatrix();



}


void fan()
{
    glPushMatrix();
    glColor3f(1, 1, 1);            // Set color as glColor3f(R,G,B)
//    glRotatef(a, 0, 0, 1);
    glRecti(-50, -50, 50, 50);
    glColor3f(0, 0, 1); //uper side pankha
    glRecti(-25, 50, 25, 300);
    glColor3f(1, 0, 0);
    glRecti(-300, -25, -50, 25);
    glColor3f(0.4, 0, 0);
    glRecti(-25, -300, 25, -50);
    glColor3f(0.4, 0.2, 0.6);
    glRecti(50, -25, 300, 25);
    glPopMatrix() ;


}

void fish1()
{
 glColor3f(1.0,0.5,0.3);//red fish
  glBegin(GL_POLYGON);

   glVertex3f(0,0,0);
   glVertex3f(5,-3,0);
   glVertex3f(10,0,0);

   glVertex3f(12,1,0);
   glVertex3f(12,-1,0);
   glVertex3f(10,0,0);

   glVertex3f(5,3,0);

  glEnd();


    glPushMatrix();
    glColor3f(0,0,0);
	glTranslatef(2.5,0,0);
	glScalef(.15,.15,0);
	circle(4);
	glPopMatrix();

  ;
}

void fish()
{
	glPushMatrix();
	//glColor3f (.5, .5 ,.5);
	glScalef(.7,.7,0);
	glTranslatef(50,24,0);
	fish1();
	glPopMatrix();

}

void stick_fan()
{
	glPushMatrix();
	glBegin(GL_POLYGON);
	glColor3f(1, 1,1) ;
	glVertex3f (0, 0, 0.0);
    glVertex3f (0,-20, 0.0);
    glVertex3f (5,-20, 0.0);
    glVertex3f (10,0, 0.0);
    glPopMatrix() ;

}



void display(void){

	glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();
	glTranslatef(v_back_ghop+10,15,0);
	glScalef(.3,.3,0);
	jhop();
	glPopMatrix();

    glPushMatrix();
	glTranslatef(v_back_ghop+20,15,0);
	glScalef(.3,.3,0);
	jhop();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(v_back_ghop,15,0);
	glScalef(.3,.3,0);
	jhop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop+30,15,0);
	glScalef(.3,.3,0);
	jhop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop,15,0);
	glScalef(.3,.3,0);
	jhop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-10,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-20,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-30,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-40,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-50,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-60,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-70,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-80,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-90,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

    glPushMatrix();
	glTranslatef(v_back_ghop-100,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-110,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();


	glPushMatrix();
	glTranslatef(v_back_ghop-120,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(v_back_ghop-130,15,0);
	glScalef(.3,.3,0);
	jhop();
	//back ghop();
	glPopMatrix();


	glPushMatrix();
	ground();
	glPopMatrix();

	//right

	glPushMatrix();
	glColor3f (1, 1 ,1);
	glTranslatef(v_cloud_r,27,0);
	cloud();
	glPopMatrix();

	//left

	glPushMatrix();
	glColor3f (1, 1 ,1);
	glTranslatef(v_cloud_l,27,0);
	cloud();
	glPopMatrix();

	//fish
	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+10,fish_y,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+20,fish_y-4,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x,fish_y-4,0);
	fish();
	glPopMatrix();

	//fish-2
	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+60,fish_y,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+80,fish_y-4,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+190,fish_y-5,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+200,fish_y-1,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+210,fish_y-5,0);
	fish();
	glPopMatrix();

    glPushMatrix();
	glColor3f (.5, .5 ,.5);
	glTranslatef(fish_x+70,fish_y-2,0);
	fish();
	glPopMatrix();

	glPushMatrix();
	glColor3f (1, 1 ,1);
	glTranslatef(13,0,0);
	glPopMatrix();



    //tree
	glPushMatrix();
	glTranslatef(-70,4,0);
	glScalef(1.2,1.2,0);
	tree();
	glPopMatrix();

    //bush
	glPushMatrix();
	glColor3f (0.137255,0.556863,0.137255);
	glTranslatef(-55,-9,0);
	glScalef(.7,.6,0);
	jhop();
	glPopMatrix();

	 //tree
	glPushMatrix();
	glColor3f (0, 1 ,0);
	glTranslatef(-25,4,0);
	glScalef(1.2,1.2,0);
	tree();
	glPopMatrix();

    //ghor

	glPushMatrix();
	glTranslatef(30,20,0);
	home();
	glPopMatrix();

    //fan

	glPushMatrix();
	glTranslatef(-10,10,0);
	glScalef(.05,.05,0);
	glRotatef(v_fan, 0, 0, 1);
    fan();
    glPopMatrix();

	//boat
	glPopMatrix();
	glPushMatrix();
	glTranslatef(v_boat,-20,0);
	boat();
	glPopMatrix();
	glutSwapBuffers();
    }


void spinDisplay(void)
{
	if(flag==1)
	{
		v_boat = v_boat - 0.1;
		if (v_boat < -100.0)
		{
			v_boat = 70 ;

		}
	}

	 v_cloud_r = v_cloud_r - 0.09;
	 if (v_cloud_r < -50)
		v_cloud_r = 50;

	 v_cloud_l = v_cloud_l + 0.09;
	 if (v_cloud_l > 50)
		v_cloud_l = -50;



	 v_fan = v_fan + 5;
	 if(v_fan>360)
		v_fan = 0;

    fish_x = fish_x - 0.05;
	 if(fish_x>100)
		{
		    fish_x = 10;
        }

	fish_y = fish_y - 0.05;
	 if(fish_y<-42)
		{
		    fish_y = -25;
        }
	glutPostRedisplay();
}


void init(void)
{

	glClearColor(0.196078, 0.6, 0.8, 0.0f);				// Black Background

	glClearDepth(1.0f);									// Depth Buffer Setup

	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

}


void reshape (int w, int h)
{
    glViewport (0, 0, w, h);

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective(45.0, (GLfloat) w/(GLfloat) h, 1.0, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	gluLookAt (0.0, 0.0, 100.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

}


void mouse(int button, int state, int x, int y)
{
   switch (button)
   {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
		 {
			//flag=1;
			 glutIdleFunc(spinDisplay);

		 }
			 break;
}

   }



int main()
{
   //glutInit(&argc, argv);
   //printf("\n\t\tPress mouse left button to rotate and right button to stop.\n");

   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize (1100, 600);
   glutInitWindowPosition (100, 100);
   glutCreateWindow ("Welcome to our Final Graphics Project Design,Planning and Coding By Sabbir Ahmed, Zubair Hossain Sami, Kaniz Rabeya Khan");
   init ();
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutMouseFunc(mouse);
   glutMainLoop();
   return 0;
}
